import pytest
import testit


@testit.workItemID(15)
@testit.displayName('Best_autotest_2')
@testit.externalID('Best_autotest_2')
@testit.link(type=testit.LinkType.DEFECT, url='https://roviti2348.atlassian.net/browse/JCP-15593')
@pytest.mark.skip('no way of currently testing this')
def test_6():
	with testit.step('Unreachable step 1'):
		with testit.step('Unreachable step 1.1'):
			assert True
		with testit.step('Unreachable step 1.2'):
			assert True
	with testit.step('Unreachable step 2'):
		assert True
