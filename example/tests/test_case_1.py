from os.path import join, dirname

import pytest
import testit
#import pytest_check


class TestClass1:
	def setup_method(self):
		with testit.step('Open Chrome browser'):
			pass

	def teardown_method(self):
		with testit.step('Close Chrome browser'):
			pass

	@testit.workItemID(13)
	@testit.displayName('Authorization test [{test_suite}]')
	@testit.externalID('authorization_test_{test_suite}')
	@testit.title('Authorization')
	@testit.description('E2E_autotest')
	@testit.labels('{labels}')
	@testit.link(type='{link_type}', url='{url}', title='{link_title}')
	@testit.link(type=testit.LinkType.REQUIREMENT, url='https://best-tms.testit.software/projects')
	@testit.link(type=testit.LinkType.BLOCKED_BY, url='https://testit.software/')
	@testit.link(type=testit.LinkType.REPOSITORY, url='https://git.testit.software/users/sign_in')
	@pytest.mark.parametrize('test_suite, username, password, labels, url, link_type, link_title', [
		('suite 1', 'admin', 'Tester123Tester', ['E2E', 'Auth'], 'https://dumps.example.com/module/JCP-15593', testit.LinkType.DEFECT, 'JCP-15593'),
		('suite 2', 'admin', 'Qwerty123', (), 'https://github.com/testit-tms/autotest-integration-python', testit.LinkType.RELATED, 'Integration python')
	])
	def test_authorization(self, test_suite, username, password, labels, url, link_type, link_title):
		testit.addLink(url="http://best-tms.testit.software/", title="Тестируемый продукт")
		authorization(username, password)
		with testit.step('Create a project', 'the project was created'):
			with testit.step('Enter the project', 'the contents of the project are displayed'):
				assert True
			with testit.step('Create a section', 'section was created'):
				assert True
			with testit.step('Create a test case', 'test case was created'):
				assert True#pytest_check.is_false(True)


class TestClass2:
	def setup_method(self):
		with testit.step('Open Chrome browser'):
			pass

	def teardown_method(self):
		with testit.step('Close Chrome browser'):
			pass

	@testit.workItemID(14)
	@testit.displayName('Load files test')
	@testit.externalID('load_files_test')
	@testit.title('Attachments')
	@testit.description('E2E_autotest')
	@testit.labels('E2E', 'File')
	def test_load_files(self):
		testit.addLink(url="http://best-tms.testit.software/", title="Тестируемый продукт")
		authorization('admin', 'Qwerty123')
		testit.attachments(join(dirname(__file__), 'pictures/picture.jpg'))
		with testit.step('Attachments'):
			load_attachment(join(dirname(__file__), 'pictures/picture.jpg'))
			load_attachment(join(dirname(__file__), 'docs/document.docx'))
			load_attachment(join(dirname(__file__), 'docs/logs.log'))
			load_attachment(join(dirname(__file__), 'docs/text_file.txt'))
			load_attachment(join(dirname(__file__), 'docs/document.doc'))


@testit.step('Log in the system', 'system authentication')
def authorization(username, password):
	assert set_login(username)
	assert set_password(password)


@testit.step
def set_login(username):
	return 'admin' == username


@testit.step
def set_password(password):
	return 'Qwerty123' == password


@testit.step
def load_attachment(file):
	testit.attachments(file)
